//  RUN UNDER NODE 14

var sensor = require("node-dht-sensor");

sensor.read(22, 27, function (err, temperature, humidity) {
    if (!err) {
        console.log(`temp: ${temperature}°C, humidity: ${humidity}%`);
    }
    else {
        console.error(err);
    }
});