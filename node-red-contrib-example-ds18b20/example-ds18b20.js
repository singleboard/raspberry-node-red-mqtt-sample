module.exports = function (RED) {
    // example-ds18b20 ExampleDs18b20
    function ExampleDs18b20Node(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', function (msg) {
            msg.payload = msg.payload.toLowerCase();
            node.send(msg);
        });
    }
    RED.nodes.registerType("example-ds18b20", ExampleDs18b20Node);
}