// run NODE 10

// AFTER RELOAD EXECUTE
// `sudo modprobe w1-gpio && sudo modprobe w1_therm`
// ls -l /sys/bus/w1/devices/

const sensor = require('ds18b20-raspi');

const deviceId = '28-01191ab7fcc9';
let temp = sensor.readC(deviceId);
console.log(temp);


const deviceId2 = '28-3c01d6078b4d';
temp = sensor.readC(deviceId2);
console.log(temp);

// // round temperature reading to 1 digit
// const tempC = sensor.readSimpleC(1);
// console.log(`${tempC} degC`);


// // async version
// sensor.readSimpleC((err, temp) => {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log(`${temp} degC`);
//     }
// });

// // round temperature reading to 1 digit
// sensor.readSimpleC(1, (err, temp) => {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log(`${temp} degC`);
//     }
// });