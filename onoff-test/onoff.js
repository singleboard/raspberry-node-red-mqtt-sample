
// WORK ONLY ON NODEJS 10
const Gpio = require('onoff').Gpio;
const relay = new Gpio(17, 'out');

function motor_on(gpio) {
    gpio.writeSync(1);
}

function motor_off(gpio) {
    gpio.writeSync(0);
}

motor_on(relay);
setTimeout(function () {
    motor_off(relay);
    setTimeout(function () {
        relay.unexport();
    }, 2000);
}, 2000);