var rpio = require('rpio');

/* Configure P15 as an output pin, setting its initial state to low */
rpio.open(15, rpio.OUTPUT, rpio.LOW);

/* Set the pin high every 10ms, and low 5ms after each transition to high */
setInterval(function () {
    rpio.write(17, rpio.HIGH);
    setTimeout(function () {
        rpio.write(17, rpio.LOW);
    }, 2000);
}, 1000);