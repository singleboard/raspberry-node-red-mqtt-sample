# 

> Raspbian 10 (Debian buster)

```bash
# install nvm
#  

```

## MOSQUITTO
### Install MOSQUITTO
[How to install and setup](https://robot-on.ru/articles/ystanovka-mqtt-brokera-mosquitto-raspberry-orange-pi)

```bash
# add repo
sudo wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
sudo apt-key add mosquitto-repo.gpg.key
cd /etc/apt/sources.list.d/
sudo wget http://repo.mosquitto.org/debian/mosquitto-buster.list
sudo apt-get update

# install server
sudo apt-get install -y mosquitto

# install client
sudo apt-get install -y mosquitto mosquitto-clients
```

### Setup mosquitto
update `/etc/mosquitto/mosquitto.conf`

```txt
# Place your local configuration in /etc/mosquitto/conf.d/
#
# A full description of the configuration file is at
# /usr/share/doc/mosquitto/examples/mosquitto.conf.example

pid_file /var/run/mosquitto.pid

persistence true
persistence_location /var/lib/mosquitto/

log_dest topic

log_type error
log_type warning
log_type notice
log_type information

connection_messages true
log_timestamp true

include_dir /etc/mosquitto/conf.d
```

### Test MOSQUITTO
1-st terminal `mosquitto_sub -d -t hello/world`
2-nd terminal `mosquitto_pub -d -t hello/world -m "Hello from Terminal window 2!"`


## NODE RED
[How to](https://nodered.org/docs/getting-started/raspberrypi)

## Install NODE RED 
```bash
sudo apt install build-essential git

bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)

# Due to the limited memory of the Raspberry Pi, 
# you will need to start Node-RED with an additional argument to tell the underlying Node.js process 
# to free up unused memory sooner than it would otherwise.
node-red-pi --max-old-space-size=256

sudo systemctl enable nodered.service
```

*****


## Install plugins for node red

### Install 
Install the BCM2835 library
```bash
cd ~                  
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.58.tar.gz                       
tar xvfz bcm2835-1.58.tar.gz;                      
cd bcm2835-1.58;                       
./configure;                      
make;        
sudo make install
```


### node-red-contrib-sensor-ds18b20

### node-red-contrib-ui-time-scheduler

### [node-red-dashboard](https://flows.nodered.org/node/node-red-dashboard)
